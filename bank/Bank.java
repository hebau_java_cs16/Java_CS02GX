package bank;
import java.util.Scanner;
public class Bank {
	private static String bankName="中国银行";
	private String name;
	private String password;
	private double balance;
	private double turnover;
	public static void welcome(){ 
		System.out.println("欢迎来到"+bankName);
	}
	public Bank(){}
	public Bank(String name,String password,double turnover){
		this.name=name;
		this.password=password;
		this.turnover=turnover;
		this.balance=turnover-10;
	}
	public void setName(String name){
		this.name=name;
	}
	public String getName(){
		return name;
	}
	public void setPassword(String password){
		this.password=password;
	}
	public String getPassword(){
		return password;
	}
	public void setTurnover(double turnover){
		this.turnover=turnover;
	}
	public double getTurnover(){
		return turnover;
	}
	public String toString() {
		return this.name+"开户成功，账户余额"+this.balance;
	}
	public void deposit(){
		System.out.println("请输入存款金额：");
		Scanner input=new Scanner(System.in);
		double money=input.nextDouble();
		balance=balance+money;
		System.out.println(name+"您好，您的账户已存入"+money+"元，当前余额"+balance+"元");
	}
	public void withdrawal(){
		System.out.println("请输入取款密码：");
		Scanner input=new Scanner(System.in);
		String password=input.next();
		if(this.password.equals(password))
		{
			System.out.println("请输入取款金额：");
			Scanner input1=new Scanner(System.in);
			double money=input1.nextDouble();
			if(money<=balance)
			{
				balance=balance-money;
				System.out.println(name+"您好，您的账户已取出"+money+"元，当前余额"+balance+"元");
			}
			else
			{
				System.out.println("对不起，账户余额不足");
			}
		}
		else
		{
			System.out.println("密码错误");
		}
	}
	public static void welcomeNext(){
		System.out.println("请携带好随身物品，欢迎下次光临");
	}
}
