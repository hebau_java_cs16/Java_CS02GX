package javatwo;

public class Player implements Comparable<Player> {
	private String num,name;
	private double score;
	public Player() {}
	public Player(String num,String name,double score){
		this.setNum(num);
		this.setName(name);
		this.setScore(score);
	} 
	public void setNum(String n){
		num=n;
	}
	public void setName(String na){
		name=na;
	}
	public void setScore(double s){
		score=s;
	}
	public String getNum(){
		return num;
	}
	public String getName(){
		return name;
	}
	public double getScore(){
		return score;
	}
	public String toString(){
		return "num:"+this.num+" name:"+this.name+" score:"+this.score;
	}
	public int compareTo(Player o) { 
        if (this.score > o.score) { 
            return -1;   
      } else if (this.score < o.score) { 
         return 1; 
    } else { 
         return 0; 
   } 
} 
}