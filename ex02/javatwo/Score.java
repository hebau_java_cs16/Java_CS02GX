package javatwo;
import java.util.Scanner;
import java.util.Arrays;
public class Score {
	private  int number;
	private  double score[];
	public Score(){}
	public Score(int number){
		this.setNumber(number);
	}
	public void setNumber(int n){
		number=n;
	}
	public void setScore(double s[]){
		for(int i=0;i<s.length;i++)
		{
			score[i]=s[i];
		}
	}
	public int getNumber(){
		return number;
	}
	public double[] getScore(){
		return score;
	}
	public  void inputScore(){
		score=new double[number];
		for(int j=0;j<score.length;j++)
		{
			Scanner input=new Scanner(System.in);
			score[j]=input.nextDouble();
		}
		
	}
	public  double average(double score[]){
		Arrays.sort(score);
		double s=0;
		for(int i=1;i<score.length-1;i++)
		{
			s=s+score[i];
		}
		double ave=s/(number-2);
		return ave;
	}
}