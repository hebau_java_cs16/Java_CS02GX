package company;//（xxxx-xx-xx）

public class Date {
	private String year;
	private String month;
	private String day;
	private Employee employee;
	public Date(String year,String month,String day) {
		this.year=year;
		this.month=month;
		this.day=day;
	}
	public void setYear(String year) {
		this.year=year;
	}
	public void setMonth(String month){
		this.month=month;
	}
	public void setDay(String day){
		this.day=day;
	}
	public String getYear() {
		return year;
	}
	public String getMonth(){
		return month;
	}
	public String getDay(){
		return day;
	}
	public Employee getEmployee() {
		return employee; 
	}

	public void setworker(Employee employee){ 
		this.employee = employee;
	}
	public String toString(){
		return this.year+"-"+this.month+"-"+this.day+"-";
	}
}
