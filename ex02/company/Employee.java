package company;
//职工号，姓名，性别，生日，工作部门，参加工作时间
                             
public class Employee {
	private String eno;
	private String ename;
	private String esex;
	private Date ebirthday;
	private Department dept[];
	private Date etime;
	public Employee(String eno,String ename,String esex) {
		this.eno=eno;
		this.ename=ename;
		this.esex=esex;
	}
	public void setEno(String eno){
		this.eno=eno;
	}
	public void setEname(String ename) {
		this.ename=ename;
	}
	public void setEsex(String esex) {
		this.esex=esex;
	}
	public void setEbirthday(Date ebirthday) {
		this.ebirthday=ebirthday;
	}
	public void setDpet(Department[] dept) {
		this.dept=dept;
	}
	public void setEtime(Date etime) {
		this.etime=etime;
	}
	public String getEno() {
		return eno;
	}
	public String getEname() {
		return ename;
	}
	public String getEsex() {
		return esex;
	}
	public Date getEbirthday() {
		return ebirthday;
	}
	public Department[] getDept() {
		return dept;
	}
	public Date getEtime() {
		return etime;
	}
	public String toString() {
		return"职工号"+this.eno+"姓名"+this.ename+"性别"+this.esex+"生日"+this.ebirthday+"工作部门"+this.dept+"参加工作时间"+this.etime;
	}
}
