package company;	//部门编号，部门名称，经理

public class Department {
	private String dno;
	private String dname;
	private Employee manager;
	private Employee emps;
	public Department(String dno,String dname) {
		this.dno=dno;
		this.dname=dname;
		
	}
	public void setDno(String dno) {
		this.dno=dno;
	}
	public void setDname(String dname) {
		this.dname=dname;
	}
	public void setManager(Employee manager) {
		this.manager=manager;
	}
	public void setEmps(Employee emps) {
		this.emps=emps;
	}
	public String getDno() {
		return dno;
	}
	public String getDname() {
		return dname;
	}
	public Employee getManager() {
		return manager;
	}
	public Employee getEmps() {
		return emps;
	}
	public String toString() {
		return "部门编号"+this.dno+"部门名称"+this.dname+"经理"+this.manager+"员工"+this.emps;
	}
}
