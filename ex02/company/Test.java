package company;	//对象数组，两个部门，10个员工，查询
import java.util.Scanner;

public class Test {
	public static void main(String[] args) {
	Department dept1=new Department("01","管理部");
	Department dept2=new Department("02","政治部");
	Employee emp[]= {new Employee("1001","江路","女"),
						new Employee("1002","李志强","男"),
						new Employee("1003","李芳","女"),
						new Employee("1004","王敏","女"),
						new Employee("1005","黄勇","男"),
						new Employee("2001","刘宏浩","男"),
						new Employee("2002","王佳","女"),
						new Employee("2003","刘方晨","男"),
						new Employee("2004","张可立","男"),
						new Employee("2005","李冰","女")};
	Date ebirthday[]= {new Date("1984","06","04"),
					new Date("1982","8","2"),
					new Date("1975","7","7"),
					new Date("1980","10","6"),
					new Date("1990","3","24"),
					new Date("1987","2","17"),
					new Date("1987","11","8"),
					new Date("1991","5","4"),
					new Date("1988","4","26"),
					new Date("1979","9","29")};
	Date etime[]= {new Date("2011","6","14"),
			new Date("2003","8","12"),
			new Date("1992","7","17"),
			new Date("2000","1","16"),
			new Date("2015","3","4"),
			new Date("2011","12","1"),
			new Date("2008","1","8"),
			new Date("2015","5","24"),
			new Date("2003","4","2"),
			new Date("1991","9","9")};
	for(int i=0;i<10;i++){
		emp[i].setEbirthday(ebirthday[i]);
	}
	for(int i=0;i<10;i++){
		emp[i].setEtime(etime[i]);
	}
	dept1.setManager(emp[3]);
	dept2.setManager(emp[6]);
	dept1.setEmps(new Employee[] {emp[0],emp[1],emp[2],emp[3],emp[4]});
	dept2.setEmps(new Employee[] {emp[5],emp[6],emp[7],emp[8],emp[9]});
	emp[0].setDept(dept1);
	emp[1].setDept(dept1);
	emp[2].setDept(dept1);
	emp[3].setDept(dept1);
	emp[4].setDept(dept1);
	emp[5].setDept(dept2);
	emp[6].setDept(dept2);
	emp[7].setDept(dept2);
	emp[8].setDept(dept2);
	emp[9].setDept(dept2);
	for(int i=0;i<10;i++){
		System.out.printf(emp[i].toString());
		System.out.printf("    ");
		System.out.printf("生日：");
		System.out.printf(ebirthday[i].toString());
		System.out.printf("    ");
		System.out.printf("工作时间：");
		System.out.printf(etime[i].toString());
		System.out.printf("    ");
		System.out.println("所在部门：  "+emp[i].getDept().toString());
		System.out.println(emp[i].getEname()+"的部门经理:"+emp[i].getDept().getManager().toString());
		System.out.printf("\n");
	}
		System.out.printf("\n");
		System.out.printf( dept1+"\t"+"部门经理:"+emp[3].getDept().getManager().toString()+"\n"+"员工：" );
		System.out.printf("\n");
		for(int i=0;i<dept1.getEmps().length;i++) {
			System.out.printf(dept1.getEmps()[i].toString()	);
			System.out.printf("    ");
			System.out.printf("生日：");
			System.out.printf(ebirthday[i].toString());
			System.out.printf("    ");
			System.out.printf("工作时间：");
			System.out.printf(etime[i].toString());
			System.out.printf("    ");
			System.out.printf("\n");
		}
		System.out.printf("\n");
		System.out.printf( dept2+"\t"+"部门经理:"+emp[6].getDept().getManager().toString()+"\n"+"员工：" );
		System.out.printf("\n");
		for(int i=0;i<dept2.getEmps().length;i++) {
			System.out.printf(dept2.getEmps()[i].toString()	);
			System.out.printf("    ");
			System.out.printf("生日：");
			System.out.printf(ebirthday[i].toString());
			System.out.printf("    ");
			System.out.printf("工作时间：");
			System.out.printf(etime[i].toString());
			System.out.printf("    ");
			System.out.printf("\n");
		}		

}
}
