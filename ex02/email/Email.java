package email;
import java.util.Scanner;
public class Email {
	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("请输入一个Email地址：");
		String email=input.nextLine();
		if(email.indexOf("@")!=-1 && email.indexOf(".")!=-1 && email.indexOf("@")<email.indexOf("."))
		{
			if(!email.startsWith("@"))
			{
				if(email.endsWith("com")||email.endsWith("cn")||email.endsWith("net")||email.endsWith("gov")||email.endsWith("edu")||email.endsWith("org"))
				{
					System.out.println("地址正确！");
				}
				else
				System.out.println("地址请以com、cn、net、gov、edu 或 org结尾。");
			}
			else
			System.out.println("地址不能以 @ 开头，请输入正确地址。");
		}
		else
		System.out.println("地址必须同时包括字符‘@’和‘.’，并且‘@’应在‘.’之前");
	} 
}
