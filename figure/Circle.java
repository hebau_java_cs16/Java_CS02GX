package figure;
                   //圆形
public class Circle extends Plane {
	double r;
	Circle (){}
	Circle(double r){
		this.r=r;
	}
	public double getR() {
		return r;
	}
	public double getPerimeter() {
		return 2*3.14*r;
	}
	public double getArea() {
		return 3.14*r*r;
	}
}
