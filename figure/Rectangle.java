package figure;
                  //矩形
public class Rectangle extends Plane {
	double a,b;
	Rectangle(double a,double b){
		this.a=a;
		this.b=b;
	}
	public double getPerimeter() {
		return 2*a+2*b;
	}
	public double getArea() {
		return a*b;
	}
}
