package figure;
                  //圆柱体
public class Pillar extends Solid {
	double height;
	Plane circle;
	Pillar(Plane circle,double height){
		this.circle=circle;
		this.height=height;
	}
	public double getSurface() {
		return 2*circle.getArea()+circle.getPerimeter()*height;
	}
	public double getVolume() {
		if(circle==null) {
			System.out.println("没有底，无法计算体积");
			return -1;
		}
		else 
			return circle.getArea()*height;
	}
}
