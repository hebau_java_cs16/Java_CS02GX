package figure;
                              //平面图形
public abstract class Plane {
	public abstract double getPerimeter();
	public abstract double getArea();
}
