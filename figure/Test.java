package figure;
import java.util.Random;
import java.util.Scanner;
public class Test {
	public static Plane p=null;
	public static Solid s=null;
	public static void print(double a,double b){     //输出平面图形信息
		if(p.getPerimeter()==a)
			System.out.println("周长计算正确");
		else
			System.out.println("周长计算错误，答案为："+p.getPerimeter());
		if(p.getArea()==b)
			System.out.println("面积计算正确");
		else
			System.out.println("面积计算错误，答案为："+p.getArea());
	}
	public static void print1(double a,double b){          //输出立体图形信息
		if(s.getSurface()==a)
			System.out.println("表面积计算正确");
		else
			System.out.println("表面积计算错误，答案为："+s.getSurface());
		if(s.getVolume()==b)
			System.out.println("体积计算正确");
		else
			System.out.println("体积计算错误，答案为："+s.getVolume());
	}
	public static void main(String[] args) {
		int r=(int)(Math.random()*10);                //圆形
		p=new Circle(r);
		System.out.println("1、圆的半径为"+r+",请计算周长和面积");
		Scanner input=new Scanner(System.in);
		double per=input.nextDouble();
		double area=input.nextDouble();
		print(per,area);
		
		System.out.println("2、已知球的半径为"+r+",请计算表面积和体积");  //球
		s=new Sphere(r);
		double sur=input.nextDouble();
		double vol=input.nextDouble();
		print1(sur,vol);
		
		int h=(int)(Math.random()*10);            //圆柱
		System.out.println("3、已知圆柱的底面半径为"+r+"高为"+h+",请计算表面积和体积");
		s=new Pillar(p,h);
		double sur1=input.nextDouble();
		double vol1=input.nextDouble();
		print1(sur1,vol1);
		
		int h1=(int)(Math.random()*10);           //圆锥
		double l=Math.sqrt(h1*h1+r*r);
		System.out.println("4、已知圆锥的底面半径为"+r+"高为"+h1+"母线长为"+l+",请计算表面积和体积");
		s=new Cone(p,h1,l);
		double sur2=input.nextDouble();
		double vol2=input.nextDouble();
		print1(sur2,vol2);
		
		int a=(int)(Math.random()*10);              //矩形
		int b=(int)(Math.random()*10);
		p=new Rectangle(a,b);
		System.out.println("5、请计算边长为"+a+"、"+b+"的矩形的周长和面积");
		double per1=input.nextDouble();
		double area1=input.nextDouble();
		print(per1,area1);
		
		int a1=(int)(Math.random()*10);           //三角形
		int b1=(int)(Math.random()*10);
		int c1=(int)(Math.random()*10);
		int h2=(int)(Math.random()*10);
		p=new Triangle(a1,b1,c1,h2);
		System.out.println("6、请计算边长为a="+a1+"、b="+b1+"、c="+c1+"，a边高为"+h2+"的三角形的周长和面积");
		double per2=input.nextDouble();
		double area2=input.nextDouble();
		print(per2,area2);
	}
}