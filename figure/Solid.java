package figure;
                            //立体图形
public abstract class Solid {
	public abstract double getSurface();
	public abstract double getVolume();
}
