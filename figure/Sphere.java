package figure;
                     //球体
public class Sphere extends Solid {
	double r;
	Sphere(double r){
		this.r=r;
	}
	public double getSurface() {
		return 4*3.14*r*r;
	}
	public double getVolume() {
		return 3.14*r*r*r*4/3;
	}
}
