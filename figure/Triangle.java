package figure;
                        //三角形
public class Triangle extends Plane {
	double a,b,c,h;
	Triangle(double a,double b,double c,double h){
		this.a=a;
		this.b=b;
		this.c=c;
		this.h=h;
	}
	public double getPerimeter() {
		return a+b+c;
	}
	public double getArea() {
		return a*h/2;
	}
}
