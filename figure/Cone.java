package figure;
                      //圆锥体
public class Cone extends Solid {
	double height;
	double line;
	Plane circle;
	Circle r=new Circle();
	Cone(Plane circle,double height,double line){
		this.circle=circle;
		this.height=height;
		this.line=line;
	}
	public double getSurface() {
		return circle.getArea()+3.14*r.getR()*line;
	}
	public double getVolume() {
		if(circle==null) {
			System.out.println("没有底，无法计算体积");
			return -1;
		}
		else 
			return circle.getArea()*height/3;
	}
}
