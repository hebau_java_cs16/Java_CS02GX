package transportation;

public class SendTask {
	private String expressNo;
	private double weight;
	public SendTask() {}
	public SendTask(String expressNo,double weight) {
		this.expressNo=expressNo;
		this.weight=weight;
	}
	public void setExpressNo(String expressNo) {
		this.expressNo=expressNo;
	}
	public String getExpressNo() {
		return expressNo;
	}
	public void setWeight(double weight) {
		this.weight=weight;
	}
	public double getWeight() {
		return weight;
	}
	public void sendBefore() {
		System.out.println("订单开始处理，仓库开始验货……\n货物重量："+this.weight+"订单已发货\n快递单号："+this.expressNo);
	}
	public void send(Transportation t,GPS tool) {
		t.setLeader("小张");
		t.setNo("zh1002");
		System.out.println("运货人"+t.getLeader()+"正在驾驶编号为"+t.getNo()+"的长城发送货物!");
		t.transport();
		tool.showCoordinate();
	}
	public void sendAfter(Transportation t) {
		t.setLeader("小张");
		t.setNo("zh1002");
		System.out.println("货物运输以完成\n运货人"+t.getLeader()+"所驾驶编号为"+t.getNo()+"的长城以归还！");
	}
}
