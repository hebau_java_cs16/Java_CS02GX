package transportation;

abstract class Transportation {
	private String no;
	private String type;
	private String leader;
	public Transportation() {}
	public Transportation(String no,String type,String leader) {
		this.no=no;
		this.type=type;
		this.leader=leader;
	}
	public void setNo(String no) {
		this.no=no;
	}
	public String getNo() {
		return no;
	}
	public void setType(String type) {
		this.type=type;
	}
	public String getType() {
		return type;
	}
	public void setLeader(String leader) {
		this.leader=leader;
	}
	public String getLeader() {
		return leader;
	}
	public abstract void transport(); 
}
