package transportation;

public class JDTransportation extends Transportation {
	public JDTransportation() {}
	public JDTransportation(String no,String type,String leader) {
		super(no,type,leader);
	}
	public void transport() {
		System.out.println("京东快递运输中");
	}
}
