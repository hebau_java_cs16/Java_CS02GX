package e;
import java.util.Scanner;
import java.util.Arrays;
public class Score {
public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    int k[][] = new int[5][10];
    System.out.println("please rate the players");
    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            k[i][j] = input.nextInt();
        }
    }
    double[] score = new double[5];
    score = average(k);
    Arrays.sort(score);
    System.out.println("the players are ranked");
    for (int i = 4; i >=0; i--) 
    {
        System.out.println(score[i]);
    }
}
public static int Max(int k[][]) {
    int a = 0;
    int max = k[a][1];
    for (int i = 0; i < 5; i++) 
    {
        for (int j = 0; j < 10; j++) 
        {
            if (k[i][j] > max)
                max = k[i][j];
        }
    }
    return max;
}
public static int Min(int k[][]) {
    int a = 0;
    int min = k[a][1];
    for (int i = 0; i < 5; i++) 
    {
        for (int j = 0; j < 10; j++) 
        {
            if (k[i][j] < min)
                min = k[i][j];
        }
    }

    return min;
}
public static double[] average(int k[][]) {

    double[] score = new double[5];

    double[] average = new double[5];

    for (int i = 0; i < 5; i++) 
    {

        for (int j = 0; j < 10; j++) 
        {

            score[i] += k[i][j];
        }

    }
    for (int i = 0; i < 5; i++) {

        score[i] = score[i] - Max(k);
        score[i] = score[i] - Min(k);
        average[i] = score[i] / 8;
    }
    return average;
}
}