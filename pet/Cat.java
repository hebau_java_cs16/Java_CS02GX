package pet;

public class Cat implements Pet {            //猫类
	private int no;
	private String kind;
	private String breed;
	private int price;
	private int num;
	public Cat(int no,String kind,String breed,int price,int num){
		this.no=no;
		this.kind=kind;
		this.breed=breed;
		this.price=price;
		this.num=num;
	}
	public void setNo(int no){
		this.no=no;
	}
	public int getNo(){
		return no;
	}
	public void setKind(String kind){
		this.kind=kind;
	}
	public String getKind(){
		return kind;
	}
	public void setBreed(String breed){
		this.breed=breed;
	}
	public String getBreed(){
		return breed;
	}
	public void setPrice(int price){
		this.price=price;
	}
	public int getPrice(){
		return price;
	}
	public void setNum(int num){
		this.num=num;
	}
	public int getNum(){
		return num;
	}
}