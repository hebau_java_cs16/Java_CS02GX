package pet;

interface Pet {            //编号，种类，品种，单价，数量
	public int getNo();
	public String getKind();
	public String getBreed();
	public int getPrice();
	public int getNum();
}