package pet;

public class Shop {
	private static Pet[] pets;        //数组，用于存放所有宠物属性
	private int f;
	public Shop(int len) {
		if(len>0) {
			this.pets=new Pet[len];
		}
		else {
			this.pets=new Pet[1];
		}
	}
	public Pet[] getPets() {
		return pets;
	}
	public boolean add(Pet pet) {         //增加宠物
		if(this.f<this.pets.length) {
			this.pets[this.f]=pet;
			this.f++;
			return true;
		}
		else {
			return false;
		}
	}
	public static void print(Pet p[]) {          //展示所有宠物
		System.out.println("编号\t种类\t品种\t价格\t数量");
		for (int i=0;i<p.length;i++) {
			if(p[i]!=null) {
				System.out.println(p[i].getNo()+"\t"+p[i].getKind()+"\t"+p[i].getBreed()+"\t"+p[i].getPrice()+"\t"+p[i].getNum());
			}
		}
	}
	public  void all() {            //添加宠物
		Shop s=new Shop(6);
		s.add(new Cat(1001, "哺乳动物", "波斯猫", 6000, 10));
		s.add(new Cat(1002, "哺乳动物", "美国短毛猫", 3000, 25));
		s.add(new Cat(1003, "哺乳动物", "折耳猫", 700, 15));
		s.add(new Dog(1004, "哺乳动物", "拉布拉多", 2000, 20));
		s.add(new Dog(1005, "哺乳动物", "德牧", 5000, 10));
		s.add(new Dog(1006, "哺乳动物", "博美", 4000, 16));
		print(s.getPets());
	}
	public Pet[] search(int no,int num) {       //返回查找到的符合用户购买的宠物的属性
		Pet ps[]=new Pet[num];
		int m=0;
		for(int j=0;j<this.pets.length;j++) {
			if(this.pets[j].getNo()==no) {
				ps[m]=this.pets[j];
				m++;
			}
		}
		return ps;
	}
}
