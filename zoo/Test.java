package zoo;
import java.util.Scanner;
public class Test {
	public static void main(String[] args) {
		System.out.println("请选择要喂的动物：\n1、狮子\n2、猴子\n3、鸽子");
		Scanner input=new Scanner(System.in);
		int n=input.nextInt();
		Feeder feeder=new Feeder();
		switch(n) {
		case 1: Animal[] lion=new Lion[1];
				lion[0]=new Lion();
				feeder.Feeder(lion);
				break;
		case 2: Animal[] monkey=new Monkey[5];
			for(int i=0;i<monkey.length;i++) {
				monkey[i]=new Monkey();
			}
				feeder.Feeder(monkey);
				break;
		case 3: Animal[] pigeon=new Pigeon[10];
			for(int i=0;i<pigeon.length;i++) {
				pigeon[i]=new Pigeon();
			}
				feeder.Feeder(pigeon);
				break;
		}
	}
}
