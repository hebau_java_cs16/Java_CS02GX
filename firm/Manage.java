package firm;

public class Manage extends Employee{
	private String duty;
	private int salary;
	public Manage(String name,String age,String sex,String duty,int salary) {
		super(name,age,sex);
		this.duty=duty;
		this.salary=salary;
	}
	public void setDuty(String duty) {
		this.duty=duty;
	}
	public String getDuty() {
		return duty;
	}
	public void setSalary(int salary) {
		this.salary=salary;
	}
	public int getSalary() {
		return salary;
	}
	public String toString() {
		return "姓名："+getName()+"\t年龄："+this.getAge()+"\t性别："+this.getSex()+"\t职务："+this.duty+"\t年薪："+this.salary;
	}
}
