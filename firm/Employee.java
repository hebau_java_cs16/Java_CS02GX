package firm;

public class Employee {
	private String name;
	private String age;
	private String sex;
	public Employee() {}
	public Employee(String name,String age,String sex) {
		this.name=name;
		this.age=age;
		this.sex=sex;
	}
	public void setName(String name) {
		this.name=name;
	} 
	public String getName() {
		return name;
	}
	public void setAge(String age) {
		this.age=age;
	}
	public String getAge() {
		return age;
	}
	public void setSex(String sex) {
		this.sex=sex;
	}
	public String getSex() {
		return sex;
	}
	public String toString() {
		return "姓名："+this.name+"\t年龄："+this.age+"\t性别："+this.sex;
	}
}
