package firm;

public class Clerk extends Manage{
	private String department;
	private int pay;
	public Clerk(String name,String age,String sex,String duty,int salary,String department,int pay) {
		super(name,age,sex,duty,salary);
		this.department=department;
		this.pay=pay;
	}
	public void setDepartment(String deprtment) {
		this.department=department;
	}
	public String getDepartment() {
		return department;
	}
	public void setPay(int pay) {
		this.pay=pay;
	}
	public int getPay() {
		return pay;
	}
	public String toString() {
		return "姓名："+getName()+"\t年龄："+getAge()+"\t性别："+getSex()+"\t职务："+getDuty()+"\t年薪："+getSalary()+"\t所属部门："+this.department+"\t月薪："+this.pay;
	}
}
