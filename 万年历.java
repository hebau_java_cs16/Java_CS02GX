package ex;
import java.util.Scanner;
public class Calendar { 
	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("please enter a year and month");
		int y=input.nextInt();
		int m=input.nextInt();
		int d,s;
		boolean l=isLeap(y);
		if(l==true)
		{
			System.out.println("Is a leap year");
		}
		else
		{
			System.out.println("Is not a leap year");
		}
		d=days(y, m);
		System.out.println(+d+" days");
		s=totalDays(y,m);
		System.out.println("It Was "+s+" days from January 1,1900");
		System.out.println("Sunday   \tMonday   \tTuesday  \tWednesday\tThursday \tFriday   \tSaturday");
		printCalender(y,m);
	}
	public static boolean isLeap(int year) {
		if(year%4==0&&year%100!=0||year%400==0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public static int days(int year,int mouth) {
		boolean a;
		int d=0;
		a=isLeap(year);
		switch(mouth){
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				d=31;
				break;
			case 2:{if(a==true){
				   d=29;
			       }
			       else{
					d=28;
			      }
			      break;
			}
			case 4:
			case 6:
			case 9:
			case 11:
				d=30;
				break;
		}
		return d;
	}
	public static int totalDays(int year,int month) {
		int i,s=0;
		boolean a;
		for(i=1900;i<year;i++)
		{
			a=isLeap(year);
			if(a==true)
			{
				s+=366;
			}
			else
			{
				s+=365;
			}
		}
		for(i=1;i<month;i++)
		{
			s+=days(year, i);
		}
		return s;
	}
	public static void printCalender(int year,int month) {
		int n=0,i,j,d,q=0,m,k;
		n+=totalDays(year, month);
		m=n%7+1;
		for(k=0;k<m;k++)
		{
			System.out.print("\t\t");
		}
		j=m;
		d=days(year, month);
		for(i=0;i<d;i++)
		{
			q++;
			System.out.print(q+"\t\t");
			j++;
			if(j%7==0)
				System.out.println();
		}
	}
}